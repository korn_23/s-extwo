#pragma once
#include<vector>
#include<iostream>

using namespace std;

class dimArray {
private:
	int *arr;
	int size;
public:
	
	dimArray() 
	{
		size = 10;
		arr = new int(10);
	}

	dimArray(int size)
	{
		this->size = size;
		arr = new int[size];
		for (int i = 0; i < size; i++) 
		{
			arr[i] = 0;
		}
	}

	dimArray(int size, int n) 
	{
		this->size = size;
		arr = new int[size];
		for (int i = 0; i < size; i++)
		{
		arr[i] = n;
		}
	}

	dimArray( dimArray& array) { // �����������
		arr = new int[array.size];
		for (int i = 0; i < array.size; i++) {
			arr[i] = array[i];
		}
		size = array.size;
	}

	dimArray(dimArray&& array) { // �����������
		arr = array.arr;
		size = array.size;
		array.arr = nullptr;
		array.size = 0;
	}

	~dimArray() {
		delete arr;
	}


	int getSize() 
	{
		return size;
	}

	int getElem(int index)
	{
		return arr[index];
	}

	void resize(int size)  // ��������� ������� 
	{
		std::vector<int> vec;
		for (int i = 0; i < this->size; i++)vec.push_back(arr[i]);
	
		if (size > this->size) //���� ����� ������ ������ 
		{
			while (vec.size() < size) 
			{
				vec.push_back(0);
			}
			this->size = vec.size();
			arr = new int[this->size];
			
			for (int i = 0; i < this->size; i++)arr[i] = vec[i];
			
		}

		if (size < this->size) //���� ����� ������ ������
		{ 
			while (vec.size() > size) 
			{
				vec.pop_back();
			}

			this->size = vec.size();
			arr = new int[this->size];

			for (int i = 0; i < this->size; i++)arr[i] = vec[i];
		}
	}


	// ���������� ���������� 
	int & operator[] (int index) 
	{ 
		return arr[index];
	}

	dimArray& operator = (const dimArray& other) { // ������������ 
				if (this == &other) {
			return *this;
		}
				delete[] arr;
		this->arr = other.arr;
		this->size = other.size;
	}
	
	dimArray& operator=(dimArray &&other) {
		delete[] arr;
		arr = other.arr;
		size = other.size;
		other.arr = nullptr;
		other.size = 0;
	}

	const bool operator ==(const dimArray& other) { // ���������
		if(this->size!=other.size) throw std::invalid_argument("different sizes");
		for (int i = 0; i < this->size; i++)if (arr[i] != other.arr[i])return false;
		return true;
	}

	const bool operator !=(const dimArray& other) { // ����������� 
		if (this->size != other.size) throw std::invalid_argument("different sizes");
		for (int i = 0; i < this->size; i++)if (arr[i] != other.arr[i])return true;
		return false;
	}

	const bool operator < (const dimArray& other) { // ������
		int min = 0;
		if (this->size > other.size)min = other.size;
		else if (other.size > this->size)min = this->size;
		else  min = this->size;
		for (int i = 0; i <min; i++)if (arr[i]>=other.arr[i])return false;
		return this->size < other.size;
	}

	const bool operator <= (const dimArray& other) { // ������ ��� ����� 
		int min = 0;
		if (this->size > other.size)min = other.size;
		else if (other.size > this->size)min = this->size;
		else  min = this->size;
		for (int i = 0; i <min; i++)if (arr[i] > other.arr[i])return false;
		return this->size < other.size;
	}

	const bool operator > (const dimArray& other) { // ������
		int min = 0;
		if (this->size > other.size)min = other.size;
		else if (other.size > this->size)min = this->size;
		else  min = this->size;
		for (int i = 0; i <min; i++)if (arr[i] <= other.arr[i])return false;
		return this->size < other.size;
	}

	const bool operator >= (const dimArray& other) { // ������ ��� �����
		int min = 0;
		if (this->size > other.size)min = other.size;
		else if (other.size > this->size)min = this->size;
		else  min = this->size;
		for (int i = 0; i <min; i++)if (arr[i] < other.arr[i])return false;
		return this->size < other.size;
	}
	
	dimArray* operator + (const dimArray& other) { //������������ 
		
		int new_size = this->size + other.size;
		
		std::vector<int>temp;
		
		for (int i = 0; i < this->size; i++)temp.push_back(arr[i]);
		for (int j = 0; j < other.size; j++)temp.push_back(other.arr[j]);

		this->arr = new int[new_size];
		this->size = new_size;

		for (int i = 0; i < this->size; i++)arr[i] = temp[i];
		
		return this;
		
	}
	

	friend std::ostream & operator << (std::ostream &s, const dimArray &d);
	friend std::istream & operator >> (std::istream &s, dimArray &d);
	
};


std::ostream& operator << (std::ostream& s,const dimArray &d) //�����
{
	s << "������ ������� :" << d.size << "\n";
	for (int i = 0; i < d.size; i++)
	{
		s << d.arr[i] << " ";
	}
	s << "\n";
	return s;
}

std::istream& operator >> (std::istream &s , dimArray &d) //����
{
	int index = 0;
	std::cout << "������� ������ �������� : ";
	s >> index;
	std::cout << "\n";
	std::cout << "������� ������� : ";
	s >> d.arr[index];
	return s;
}